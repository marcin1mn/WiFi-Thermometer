#include <Arduino.h>
#include <Wire.h>
#include <hd44780.h>
#include <hd44780ioClass/hd44780_I2Cexp.h>
#include <Adafruit_ADS1X15.h>
#include <SPI.h>
#include <Measurement.h>
#include <Device.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <WiFiClientSecure.h>

#define D13 13
#define D12 12

const int sda_pin = D13; 
const int scl_pin = D12;

const int LCD_COLS = 20;
const int LCD_ROWS = 4;
hd44780_I2Cexp lcd(0x27);
Adafruit_ADS1115 ads;

/****** WiFi Connection Details *******/
const char* ssid = "Chatka Puchatka";
const char* password = "LubiePlacki";

/******* MQTT Broker Connection Details *******/
const char* mqtt_server = "b570eefb746847198220ecfb4d84dd72.s2.eu.hivemq.cloud";
const char* mqtt_username = "destylator1";
const char* mqtt_password = "destylator1";
const int mqtt_port =8883;
WiFiClientSecure espClient;

/**** MQTT Client Initialisation Using WiFi Connection *****/
PubSubClient client(espClient);

unsigned long lastMsg = 0;
#define MSG_BUFFER_SIZE (50)
char msg[MSG_BUFFER_SIZE];

vector<Measurement> measurements;
Device* device;

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    String clientId = "ESP8266Client-";   // Create a random client ID
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (client.connect(clientId.c_str(), mqtt_username, mqtt_password)) {
      Serial.println("connected");

    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");   // Wait 5 seconds before retrying
      delay(500);
    }
  }
}

void setup_wifi() {
    delay(10);
    Serial.print("\nConnecting to ");
    Serial.println(ssid);

    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid, password);

    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
        lcd.setCursor(0, 1);
        lcd.print("Szukanie sieci Wi-Fi");
    }
    lcd.setCursor(5, 2);
    lcd.print("Polaczono!");
    delay(250);
    lcd.clear();
    
    Measurement glowica("GLOWICA     ", &ads, 0, 0);
    Measurement ovm    ("OVM         ", &ads, 1, 1);
    Measurement polka  ("10 POLKA    ", &ads, 2, 2);
    Measurement keg    ("KEG         ", &ads, 3, 3);
    measurements.push_back(glowica);
    measurements.push_back(ovm);
    measurements.push_back(polka);
    measurements.push_back(keg);
    device = new Device(measurements);
}

void publishMessage(const char* topic, String payload ){
  if (client.publish(topic, payload.c_str(), true))
      Serial.println("Message publised ["+String(topic)+"]: "+payload);
}

void setup() {
    Serial.begin(115200);
    Wire.begin(sda_pin, scl_pin);
    lcd.begin(LCD_COLS, LCD_ROWS);
    ads.setGain(GAIN_ONE);
    if (!ads.begin()) {
        Serial.println("Failed to initialize ADS.");
        while (1);
    }
    setup_wifi();
    espClient.setInsecure();
    client.setServer(mqtt_server, mqtt_port);
}

unsigned long previousMillis = 0;
const long interval = 1000;  

void loop() {
    if (!client.connected()) reconnect(); // check if client is connected
    client.loop();

    unsigned long currentMillis = millis();
    if (currentMillis - previousMillis >= interval) {
        previousMillis = currentMillis;

        Serial.println("-----------------------------------------------------------");
        device->displayMeasurements(&lcd);
        client.publish("destylator", device->getJson(), true);
        Serial.println(device->getJson());

    }
}

