#include <Arduino.h>
#include <ArduinoJson.h>
#include <vector>
#include <hd44780ioClass/hd44780_I2Cexp.h>
using namespace std;

class Device { 
    private:
        String deviceId;
        vector<Measurement> measurements;
        char messageJson[256];
    public:
        Device(vector<Measurement> measurements) {
            this->measurements = measurements;
        }
        char* getJson() {
            const size_t capacity = 1000;
            DynamicJsonDocument doc(capacity);
            doc["deviceId"] = deviceId;
            JsonArray modules = doc.createNestedArray("modules");

            for (Measurement item : measurements) {
                JsonObject module = modules.createNestedObject();
                String label = item.getLabel();
                label.trim();
                module["label"] = label;
                module["temperature"] = item.getTemperature();
            }

            serializeJson(doc, messageJson);
            return messageJson;
        }
        void displayMeasurements(hd44780_I2Cexp* lcd) {
            for(uint8_t i = 0; i < measurements.size(); i++){
                measurements[i].getMeasurement();
                measurements[i].printSerial();
                lcd->setCursor(0, i);
                lcd->print(measurements[i].getString());
            }
        }
};