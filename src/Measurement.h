#include <Arduino.h>
#include <Adafruit_ADS1X15.h>

class Measurement { 
    private:
        String label;
        String temperature;
        String unit = "*C";
        Adafruit_ADS1115* ads;
        uint8_t channel;
        uint8_t order;
        int16_t rawADC;
        float voltage;
        float resistance;
        int length;
        uint8_t samples = 10;
        float VCC = 3.3;
        int TEMPERATURENOMINAL = 25;
        int SERIESRESISTOR = 10000;
        int THERMISTORNOMINAL = 10000;
        int BCOEFFICIENT = 3950;
        
        float calculateResistance(float sensorVoltage){
            return (SERIESRESISTOR * sensorVoltage) / (VCC - sensorVoltage);
        }

        double calculateTemperature(float sensorResistance) {
            float steinhart;
            steinhart = sensorResistance / THERMISTORNOMINAL;     // (R/Ro)
            steinhart = log(steinhart);                  // ln(R/Ro)
            steinhart /= BCOEFFICIENT;                   // 1/B * ln(R/Ro)
            steinhart += 1.0 / (TEMPERATURENOMINAL + 273.15); // + (1/To)
            steinhart = 1.0 / steinhart;                 // Invert
            steinhart -= 273.15;   
            return steinhart;
        }

        void readAnalog(){
            float val = 0;
            for(int i = 0; i < samples; i++) {
                val += ads->readADC_SingleEnded(this->channel);
                delay(1);
            }

            this->rawADC = val / samples;
        }
    public:
        Measurement(String label, Adafruit_ADS1115* ads, uint8_t order, uint8_t channel) {
            this->label = label;
            this->ads = ads;
            this->channel = channel;
            this->order = order;
        }

        String getString(){
            String spaces = "";
            if(this->temperature.length() == 4) 
                spaces = "  ";
            else if(this->temperature.length() == 5) 
                spaces = " ";
            
            return label + spaces + temperature + unit;
        }

        String getLabel(){
            return this->label;
        }
        String getTemperature(){
            return this->temperature;
        }

        void getMeasurement(){
            float tmp;
            if(this->channel == 255){
                tmp = 0.0f;
            } else {
                readAnalog();
                this->voltage = ads->computeVolts(this->rawADC);
                this->resistance = calculateResistance(this->voltage);
                tmp = calculateTemperature(this->resistance);
            }

            char number[6];
            sprintf(number, "%.2f", tmp);
            this->temperature = (String)number;
        }


        
        void printSerial(){
            if(channel != 255){
                Serial.print(label + " AIN"); 
                Serial.print(channel);
                Serial.print(": ");  
                Serial.print(rawADC); 
                Serial.print("  "); 
                Serial.print(voltage,4); 
                Serial.print("V"); 
                Serial.print("  "); 
                Serial.print(temperature); 
                Serial.print("*C"); 
                Serial.print("  "); 
                Serial.println(resistance,4); 
            }
        }
};